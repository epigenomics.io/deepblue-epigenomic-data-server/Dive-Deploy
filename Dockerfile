FROM node:14.15
WORKDIR /root/
COPY /dist ./dist
COPY package*.json ./
RUN npm install

COPY bin/www ./bin/www
COPY app.js .

EXPOSE 56570

CMD ["npm", "start"]